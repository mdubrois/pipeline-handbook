---
title: "Stack"
weight: 4000
description: "The tools used by the pipeline"
pre: "<i class='fa fa-edit' ></i> . "
tags: ["WIP"]
---
The tools are the tools

<!-- ## Table of Contents
{{% children depth="1" description="true" %}} -->

{{<mermaid align="center" description="Stack">}}
graph TD
    linkStyle default interpolate basis
    %% ------------------ NODES -----------------------------------------------
    lin("OS<br>.<br>Linux")
    win("OS<br>.<br>Windows")
    rez("Env Mgmt<br>.<br>Rez")
    gitlab("Git Repository<br>.<br>GitLab")
    runners("CICD<br>.<br>Setup GitLab Runners")
    ci("Continous Integration<br>.<br>")
    cicd_quality("Continous Integration<br>.<br>Quality")
    cicd_build("Continous Integration<br>.<br>Build")
    cicd_test("Continous Integration<br>.<br>Test")
    cicd_doc("Continous Integration<br>.<br>Doc")
    it_doc("Doc<br>.<br>Define Doc Server Adress<br>Setup Nginx")
    cicd_deploy("Continous Deployment<br>.<br>Deploy / Multi-Site")
    userAuth("IT<br>.<br>Authentification Multi-OS")
    setProdEnv("Package<br>.<br>Set Prod Environment")
    ubq("Sync File<br>.<br>Ubiquity")
    sgtk("SGTK Mgmt<br>.<br>Install Shotgun Toolkit")
    sgtktemp("SGTK<br>.<br>Setup Template Path")
    sgtkfolder("SGTK<br>.<br>Setup Folder Schema")
    ocio("OCIO<br>.<br>Setup OCIO Aces & Legacy")
    %% ------------------ LINKS -----------------------------------------------
    lin --> rez
    win --> rez
    rez --> gitlab
    gitlab --> runners
    runners --> ci
    ci --> cicd_quality
    ci --> cicd_build
    ci --> cicd_test
    ci --> cicd_doc
    it_doc --> cicd_doc
    ci  --> cicd_deploy
    ci --> sgtk
    sgtk --> sgtktemp
    sgtktemp --> sgtkfolder
    sgtkfolder --> ocio
    ocio --> setProdEnv
    setProdEnv --> ubq

    subgraph GitLab - CICD
        cicd_quality
        cicd_build
        cicd_test
        cicd_doc
        cicd_deploy
    end





{{</mermaid>}}