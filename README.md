
# Get Pipeline-Handbook
## Get repository
In your fav dir
``` bash
# in your favorite coding folder.
git clone https://gitlab.com/mdubrois/pipeline-handbook
```

# Use Hugo
## Install Hugo Locally
Follow the instruction from [Hugo](https://gohugo.io/getting-started/installing/)

_With Chocolatey for Windows_

## Run Hugo - Linux
### Run Hugo Server for live Editing
```bash
# In your repository folder.
cd src
../run_server.sh
```

## Run Hugo in a Docker
```bash
# In your repository folder.
./run_docker-server.sh
```

### Docker details
```
# Launch container
docker run --rm --name hugo -itd -v $PWD:/src -p 1313:1313 -u hugo jguyomard/hugo-builder

# log in bash
docker exec -it hugo /bin/sh

# Run hugo server locally with drafts suppport
hugo server -w --buildDrafts --disableFastRender --bind=0.0.0.0

# Stop Docker
docker stop hugo && docker rm hugo
```

## Run Hugo - Windows
```
# Go into your directory and launch
C:\ProgramData\chocolatey\lib\hugo\tools\hugo.exe server -w --buildDrafts --disableFastRender --bind=0.0.0.0
# or
hugo server -w --buildDrafts --disableFastRender --bind=0.0.0.0
```

# Custom Support
## MathJax Support
This site support LaTex equation by using MathJax.
Insert the following lines for testing.
```
$$ a * (x +2) =2$$

$$ a * \sqrt{(x +2)} =2$$

$$|\vec{A}|=\sqrt{A_x^2 + A_y^2 + A_z^2}.$$(1)
```

## Mermaid Enhanced
You can declare mermaid like below. That's add an awesome title before the graph.
And a  section for showing the mermaid code just after the graph.
```
{{<mermaid align="center" description="Build Scene - Action Flow">}}
graph LR
    linkStyle default interpolate basis
    %%-------------- NODES ----------------------------------------------------
    pro_getUsdDescTemplate("Process<br>.<br>Get<br>USD Desc Template")
    pro_createUsdDesc("Process<br>.<br>Create<br>USD Desc")
    pro_pubAssetUSD("Process<br>.<br>Publish<br>USD Asset Desc<br>MyAsset.usda")
    pro_getMayaSnTempl("Process<br>.<br>Get<br>Maya Scene Template")
    pro_buildMayaSn("Process<br>.<br>Build<br>Maya Scene")
    pro_setupMayaSn("Process<br>.<br>Setup<br>Maya Scene")
    pro_publishMayaSn("Process<br>.<br>Publish<br>Maya Scene")
    %%-------------- LINKS ----------------------------------------------------
    pro_getUsdDescTemplate --> pro_createUsdDesc
    pro_createUsdDesc --> pro_pubAssetUSD
    pro_pubAssetUSD --> pro_buildMayaSn
    pro_getMayaSnTempl --> pro_buildMayaSn
    pro_buildMayaSn --> pro_setupMayaSn
    pro_setupMayaSn --> pro_publishMayaSn
    %%-------------- SUBGRAPH -------------------------------------------------
    %%-------------- CLASSES --------------------------------------------------
    classDef Process fill:#FF8ED4
    %%-------------- ASSIGN CLASSES -------------------------------------------
    class pro_getUsdDescTemplate,pro_createUsdDesc,pro_pubAssetUSD,pro_getMayaSnTempl,pro_buildMayaSn,pro_setupMayaSn,pro_publishMayaSn Process
{{</mermaid>}}
```

# Deploy
The CI deploy the site on GitLab Pages at https://mdubrois.gitlab.io/pipeline-handbook

You should wait a few minutes between build and see the static web site up to date.