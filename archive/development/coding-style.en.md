---
title: "Coding Style"
weight: 2000
description: "The developmment part, we talk about Stack, Coding, Rules, Git, etc..."
tags: ["WIP"]
---

## Description

- Toujours prefixer ces **Env Var** et ses **extra_attributes** pour identifier facile rapidement ce qui est fait par le pipeline.
- Ne jamais rajouter dans le `PYTHONPATH` des modules qui pourrait overrider les modules des DCC
    - Example : exporter, etc...
- Definir une convention facile à suivre et utiliser par la majorité des codeurs dans le monde.


### Python
We follow the same rules than [Google](https://github.com/google/styleguide/blob/gh-pages/pyguide.md)

#### Python Repository Example
* [Code](https://gitlab.com/md-shared/python-repository-example)
* [Doc](https://md-shared.gitlab.io/python-repository-example)


### C++
We follow the same rules than [Google](https://google.github.io/styleguide/cppguide.html)

#### C++ Repository Example
No example for now.
