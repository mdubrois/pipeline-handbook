---
title: "Library"
weight: 1000
description: "Description"
tags: ["multi"]
draft: false
---

| Card ||
|---|---|
| OS | windows, linux |
| Type | standalone, multi |
| DCC  | maya, houdini |


## Description

- Tool de Load/Save de Library
    - Animation pour les chars
    - Preset pour de la dyn
    - Preset de création pour les outils. pre-rempli les champs d’un outil.
    - Avoir une API
    - Avoir une interface appellable
