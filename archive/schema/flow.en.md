---
title: Flow
weight: 100
description: "The severals Flow in the Pipeline."
tags: ["WIP"]
draft: true
---

## Description
We have 3 types of Flow :

- Task Flow
- Scenario Flow
- Action Flow


## Task Flow
The Task Flow represent the dependencies between Tasks.
Below for example the Asset Character Task Flow.

{{<mermaid align="center" description="Asset Character Task Flow">}}
graph LR
    linkStyle default interpolate basis
    %%--------------NODES -----------------------------------------------------
    acarts("Art Design")
    click acarts callback "Tooltip for a callback"
    acartf("Art Facial")
    acartl("Art Look")
    acmod("Modeling")
    acmodf("Modeling Facial")
    acsurf("Surfacing")
    accfxh("Cfx Hair")
    accfxc("Cfx Cloth")
    acriglo("Rig-Low")
    acrighi("Rig-High")
    acfx("Fx")
    %%-------------- LINKS ----------------------------------------------------
    acarts --> acmod
    acarts --> acartf
    acartf --> acmodf
    acarts --> acartl
    acartl --> acsurf
    acmod --> acmodf
    acmod --> acsurf
    acmod --> acriglo
    acmod --> acrighi
    acmodf --> acrighi
    acsurf --> acrighi
    acsurf -.->|optional| acfx
    acmod -.->|optional| accfxh
    acmod --> accfxc
    accfxh --> acrighi
    acrighi -.->|optional| accfxc
    acmod -.->|optional| acfx
    acfx -.->|optional| acrighi
    %%-------------- SUBGRAPH -------------------------------------------------
    subgraph Art
        acarts
        acartf
        acartl
    end
    subgraph Modeling
        acmod
        acmodf
    end
    subgraph Cfx
        accfxh
        accfxc
    end
    subgraph Surfacing
        acsurf
    end
    subgraph Rig
        acriglo
        acrighi
    end
    subgraph Fx
        acfx
    end
    %%-------------- CLASSES --------------------------------------------------
    classDef Art fill:#49A5FF
    classDef Modeling fill:#F8D5C6
    classDef Cfx fill:#80FF9F
    classDef Surfacing fill:#F668FF
    classDef Rig fill:#FF86B2
    classDef Fx fill:#FF8C76
    %%-------------- ASSIGN CLASSES -------------------------------------------
    class acarts,acartf,acartl Art
    class acmod,acmodf Modeling
    class accfxh,accfxc Cfx
    class acsurf Surfacing
    class acriglo,acrighi Rig
    class acfx Fx
{{</mermaid>}}
Inside a `Task`, you can have multiples scenarios about everything.
In case of `modeling character` you can have :

- Build Scene - Scenario
- Open Scene - Scenario
- Export to Zbrush - Scenario
- Import from Zbrush - Scenario
- Publish for Review - Scenario
- Prepa Review - Scenario
- Post Review - Scenario

## Scenario Flow
A `Scenario` Flow describe the main step of this `Scenario`. And identify the type of step.

- User, for human input.
- Rabbit (We can find a better name), for handling message. Rabbit because it's based on [RabbitMQ](https://www.rabbitmq.com/).
- Action, for machine processes.

Below for example the Build Scene - Scenario.
{{<mermaid align="center" description="Build Scene - Scenario Flow">}}
graph LR
    linkStyle default interpolate basis
    %%-------------- NODES ----------------------------------------------------
    usr_setTaskRdy("User<br>.<br>Set SG Task<br>model = rdy")
    rbt_setTaskRdy("Rabbit<br>.<br>")
    act_buildScene("Action<br>.<br>Build Scene")
    rbt_buildScene("Rabbit<br>.<br>")
    act_notifyUser("Action<br>.<br>Notify User")
    act_syncToUserWkst("Action<br>.<br>Ubiquity Send<br>to User")
    %%-------------- LINKS ----------------------------------------------------
    usr_setTaskRdy --> rbt_setTaskRdy
    rbt_setTaskRdy --> act_buildScene
    act_buildScene --> rbt_buildScene
    rbt_buildScene --> act_notifyUser
    rbt_buildScene --> act_syncToUserWkst
    %%-------------- SUBGRAPH -------------------------------------------------
    %%-------------- CLASSES --------------------------------------------------
    classDef User fill:#FFEA6F
    classDef Rabbit fill:#FFB286
    classDef Action fill:#6FA2FF
    %%-------------- ASSIGN CLASSES -------------------------------------------
    class usr_setTaskRdy User
    class rbt_setTaskRdy,rbt_buildScene Rabbit
    class act_buildScene,act_notifyUser,act_syncToUserWkst Action
{{</mermaid>}}

## Action Flow
As seen in the `Scenario` Flow above, a `Scenario` could contain `Action`.
An `Action` is flow of `Process`.

In the example below you will see the `Process` for the Build Scene - Scenario.

{{<mermaid align="center" description="Build Scene - Action Flow">}}
graph LR
    linkStyle default interpolate basis
    %%-------------- NODES ----------------------------------------------------
    pro_getUsdDescTemplate("Process<br>.<br>Get<br>USD Desc Template")
    pro_createUsdDesc("Process<br>.<br>Create<br>USD Desc")
    pro_pubAssetUSD("Process<br>.<br>Publish<br>USD Asset Desc<br>MyAsset.usda")
    pro_getMayaSnTempl("Process<br>.<br>Get<br>Maya Scene Template")
    pro_buildMayaSn("Process<br>.<br>Build<br>Maya Scene")
    pro_setupMayaSn("Process<br>.<br>Setup<br>Maya Scene")
    pro_publishMayaSn("Process<br>.<br>Publish<br>Maya Scene")
    %%-------------- LINKS ----------------------------------------------------
    pro_getUsdDescTemplate --> pro_createUsdDesc
    pro_createUsdDesc --> pro_pubAssetUSD
    pro_pubAssetUSD --> pro_buildMayaSn
    pro_getMayaSnTempl --> pro_buildMayaSn
    pro_buildMayaSn --> pro_setupMayaSn
    pro_setupMayaSn --> pro_publishMayaSn
    %%-------------- SUBGRAPH -------------------------------------------------
    %%-------------- CLASSES --------------------------------------------------
    classDef Process fill:#FF8ED4
    %%-------------- ASSIGN CLASSES -------------------------------------------
    class pro_getUsdDescTemplate,pro_createUsdDesc,pro_pubAssetUSD,pro_getMayaSnTempl,pro_buildMayaSn,pro_setupMayaSn,pro_publishMayaSn Process
{{</mermaid>}}
