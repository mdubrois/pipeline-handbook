---
title: Asset
weight: 2500
description: "An Asset Schema from the Technical point of view."
tags: ["WIP"]
draft: false
---

## Asset Stack
Based on USD the Asset Stack should be.

{{<mermaid align="center" description="Asset Stack.">}}
graph TD
    linkStyle default interpolate basis
    %%--------------NODES -----------------------------------------------------
    asset_base("asset_base.usda")
    asset_char_base("asset_char_base.usda")
    modeling("modeling.usda")
    modeling_facial("modeling_facial.usda")
    texturing("texturing.usda")
    surfacing("surfacing.usda")
    cfx_hair("cfx_hair.usda")
    cfx_cloth("cfx_cloth.usda")
    fx("fx.usda")
    rig("rig.usda")

    fx -->cfx_cloth
    cfx_cloth --> rig
    rig --> surfacing
    surfacing --> cfx_hair
    cfx_hair --> texturing
    texturing --> modeling_facial
    modeling_facial --> modeling
    modeling --> asset_char_base
    asset_char_base --> asset_base
    subgraph Asset.usda
        asset_base
        asset_char_base
        modeling
        modeling_facial
        texturing
        surfacing
        cfx_hair
        cfx_cloth
        fx
        rig
    end
{{</mermaid>}}

## Asset Layering
WIP asset layering with USD.

{{<mermaid align="center" description="Asset Layering Workflow for a Step.">}}
graph TD
    linkStyle default interpolate basis
    %% USD Asset Spec
    atemp("Asset Template<br>Description<br>----<br>asset_template.usda")
    tasktemp("Step  Description<br>Template<br>----<br>taskX_template.usda")
    ax("Asset X<br>----<br>asset_x.usda")
    task("Step X<br>----<br>reference | Asset X<br>override | set override like unload part, hide part<br>----<br>asset_x_task_x_build_v001.usda")
    task_work("Step Work<br>----<br>Artist create/override content<br>----<br>asset_x_task_x_work_v001.ma")
    scene_pub("Publish Work Scene<br>----<br>asset_x_task_x_pub_v001.ma")
    scene_abc("ABC Scene<br>----<br>extract ABC file<br>----<br>asset_x_task_x_v001.abc")
    task_exp("Publish Step X<br>----<br>export diff content from Step X<br>reference | ABC Scene<br>----<br>asset_x_task_x_publish_v001.usda")
    ax_in_taskx_pub("Asset X<br>----<br>reference | Step X > asset_x_task_x_publish_v001.usda<br>----<br>asset_x.usda")

    atemp --> ax
    ax -->task
    tasktemp --> task
    task --> task_work

    task_work --> scene_pub
    scene_pub --> task_exp
    scene_pub --> scene_abc
    scene_abc --> task_exp
    ax --> ax_in_taskx_pub
    task_exp --> ax_in_taskx_pub
    subgraph Template
        atemp
        tasktemp
    end
    subgraph Asset Description
        ax
        ax_in_taskx_pub
    end
    subgraph Step
        task
        task_work
        subgraph Publish Process
            scene_pub
            scene_abc
            task_exp
        end
    end
{{</mermaid>}}

## Technical Parts of an Asset
The main technical part of an Asset are:

| blah | bla |
| ---- | ---- |
| asset | transform |
| asset | variants |
| mesh | vertices |
| mesh | uv |
| mesh | visibility |
| mesh | hiddenFaces |


| material.assignment
- material.override

- mesh.vertices
- mesh.uv
- mesh.transform
- mesh.visibility
- mesh.hiddenFaces
- material.assignment
- material.override
- load


## Who Create or Override a Technical Part
The graph below describe which Task create or override a part.

{{<mermaid align="center" description="Simple Shot Schema.">}}
graph LR
    linkStyle default interpolate basis
    %% USD Asset Spec
    itemvis("item.visibility")
    vis("visibility")
    load("load")
    meshv("mesh.vertices")
    meshu("mesh.uv")
    mesht("mesh.transform")
    meshh("mesh.hiddenFaces")
    mag("material.assignment")
    maovr("material.override")
    hairg("hair.groom")
    tex("textures")
    part("particles")
    vdb("vdb")


    riganm("rig.anim")
    rigcloth("rig.cloth")
    rigfxproxy("rig.fx_proxy")
    rigfx("rig.fx")

    %% Task Name
    setdress("Set Dress")
    modchar("Modeling *")
    modcharfacial("Modeling Character Facial")
    cfxh("Cfx Hair")
    cfxc("Cfx Cloth")
    text("Texturing")
    surf("Surfacing")
    rig("Rig")
    fx("Fx")

    %% Link
    toto --> mesh.vertices
    setdress -->|ovr| mesht
    setdress -->|ovr| load
    setdress -->|ovr| vis
    setdress -->|ovr| meshvis

    modchar -->|create| mesht
    modchar -->|create| meshv
    modchar -->|create| meshvis
    modchar -->|create| meshu
    modchar -->|create| mag
    modchar -->|ovr| maovr
    modchar -->|create| tex
    modcharfacial -->|create| meshv
    modcharfacial -->|create| meshu
    modcharfacial -->|ovr| meshv
    modcharfacial -->|ovr| meshu

    text -->|create| tex
    surf -->|ovr| meshu
    surf -->|ovr| mag
    surf -->|ovr| maovr
    surf -->|ovr| meshh

    cfxh -->|ovr| meshv
    cfxh -->|ovr| meshu
    cfxh -->|create| hairg

    rig -->|create| riganm
    cfxc -->|create| rigcloth
    fx -->|create| rigfxproxy
    fx -->|create| rigfx
    fx -->|create| part
    fx -->|create| vdb

    subgraph Asset Part
        subgraph common
            load
            vis
        end
        subgraph mesh
            meshv
            meshu
            mesht
            meshvis
            meshh
        end
        subgraph surfacing
            tex
            mag
            maovr
        end
        subgraph others
            vdb
            part
        end

        subgraph rig
            hairg
            riganm
            rigcloth
            rigfx
            rigfxproxy
        end
    end

    subgraph Task
        subgraph model
            modchar
            modcharfacial
        end
        subgraph surfacing
            text
            surf
        end
        subgraph rig
            rig
            cfxh
            cfxc
            fx
        end
        subgraph setdress
            setdress
        end
    end



{{</mermaid>}}


{{% panel theme="danger" header="TODO" %}}
Define Asset Structure,

- Prop
- Set
- SetDress > Bank Library

{{% /panel %}}