---
title: "Asset"
weight: 1000
description: "The Asset Zone."
tags: ["WIP"]
---

The usual Pipeline is like below.

{{<mermaid align="center" description="Simple Asset Task Schema.">}}
graph LR
    linkStyle default interpolate basis
    model("Modeling")
    surf("Surfacing")
    hair("Hair")
    cloth("Cloth")
    fx("FX")
    rig("Rig")
    subgraph Asset
        model --> surf
        surf --> rig
        model --> rig
        hair --> rig
        hair --> surf
        model --> hair
        model --> cloth
        rig --> cloth
        model --> fx
    end
    subgraph Actor
        rig --> actor_rig
        model --> actor_model
        surf --> actor_surf
        fx --> actor_fx
        cloth --> actor_cfx
    end
{{</mermaid>}}

