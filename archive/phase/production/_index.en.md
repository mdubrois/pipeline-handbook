---
title: "Production"
weight: 2000
---

The usual Pipeline is like below.

```mermaid

    graph LR
    model("Modeling")
    surf("Surfacing")
    hair("Hair")
    cloth("Cloth")
    fx("FX")
    rig("Rig")
    subgraph Asset
        model --> surf
        surf --> rig
        model --> rig
        hair --> rig
        hair --> surf
        model --> hair
        model --> cloth
        rig --> cloth
        model --> fx
    end
    subgraph Actor
        rig --> actor_rig
        model --> actor_model
        surf --> actor_surf
        fx --> actor_fx
        cloth --> actor_cfx
    end

```