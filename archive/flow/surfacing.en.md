---
title: Surfacing
weight: 1000
description: "The workflow for the Modeling Task"
tags: ["WIP"]
draft: true
---

## Status

## Scenario Flow
Inside a `Task`, you can have multiples scenarios about everything.
In case of `modeling character` you can have :

- Build Scene - Scenario
- Open Scene - Scenario
- Export to Zbrush - Scenario
- Import from Zbrush - Scenario
- Publish for Review - Scenario
- Prepa Review - Scenario
- Post Review - Scenario

In a `Scenario`, your Flow contain different `node` of type :

- User, for human input.
- Rabbit (We can find a better name), for handling message. Rabbit because it's based on [RabbitMQ](https://www.rabbitmq.com/).
- Action, for machine processes.

Below the different scenarios exposed before.
{{<mermaid align="center" description="Build Scene - Scenario">}}
graph LR
    linkStyle default interpolate basis
    %%-------------- NODES ----------------------------------------------------
    usr_setTaskRdy("User<br>.<br>Set SG Task<br>model = rdy")
    rbt_setTaskRdy("Rabbit<br>.<br>")
    act_buildScene("Action<br>.<br>Build Scene")
    rbt_buildScene("Rabbit<br>.<br>")
    act_notifyUser("Action<br>.<br>Notify User")
    act_syncToUserWkst("Action<br>.<br>Ubiquity Send<br>to User")
    %%-------------- LINKS ----------------------------------------------------
    usr_setTaskRdy --> rbt_setTaskRdy
    rbt_setTaskRdy --> act_buildScene
    act_buildScene --> rbt_buildScene
    rbt_buildScene --> act_notifyUser
    rbt_buildScene --> act_syncToUserWkst
    %%-------------- SUBGRAPH -------------------------------------------------
    %%-------------- CLASSES --------------------------------------------------
    classDef User fill:#FFEA6F
    classDef Rabbit fill:#FFB286
    classDef Action fill:#6FA2FF
    %%-------------- ASSIGN CLASSES -------------------------------------------
    class usr_setTaskRdy User
    class rbt_setTaskRdy,rbt_buildScene Rabbit
    class act_buildScene,act_notifyUser,act_syncToUserWkst Action
{{</mermaid>}}

{{<mermaid align="center" description="Open Scene - Scenario">}}
graph LR
    linkStyle default interpolate basis
    %%-------------- NODES ----------------------------------------------------
    usr_goInProdenv("User<br>.<br>Go in Prod Env")
    act_setProdEnv("Action<br>.<br>Set Prod Env")
    usr_launchMaya("User<br>.<br>Launch<br>Maya")
    act_launchMaya("Action<br>.<br>Launch<br>Maya")
    usr_launchSnMan("User<br>.<br>Launch<br>Scene Manager")
    act_sceneManager("Action<br>.<br>Scene Manager")
    usr_openPublishedScene("User<br>.<br>Open<br>Published Scene<br>as Work")
    usr_openWorkScene("User<br>.<br>Open<br>Work Scene")
    act_openPublishedScene("Action<br>.<br>Open<br>Published Scene<br>as Work")
    act_openWorkScene("Action<br>.<br>Open<br>Work Scene")
    %%-------------- LINKS ----------------------------------------------------
    usr_goInProdenv --> act_setProdEnv
    act_setProdEnv --> usr_launchMaya
    usr_launchMaya --> act_launchMaya
    act_launchMaya --> usr_launchSnMan
    usr_launchSnMan --> act_sceneManager
    act_sceneManager --> usr_openPublishedScene
    act_sceneManager --> usr_openWorkScene
    usr_openPublishedScene --> act_openPublishedScene
    usr_openWorkScene --> act_openWorkScene
    %%-------------- SUBGRAPH -------------------------------------------------
    %%-------------- CLASSES --------------------------------------------------
    classDef User fill:#FFEA6F
    classDef Rabbit fill:#FFB286
    classDef Action fill:#6FA2FF
    %%-------------- ASSIGN CLASSES -------------------------------------------
    class usr_goInProdenv,usr_launchMaya,usr_launchSnMan,usr_openPublishedScene,usr_openWorkScene User
    class rbt_setTaskRdy,rbt_buildScene Rabbit
    class act_setProdEnv,act_launchMaya,act_sceneManager,act_openPublishedScene,act_openWorkScene Action
{{</mermaid>}}

{{<mermaid align="center" description="Export to Zbrush - Scenario">}}
graph LR
    linkStyle default interpolate basis
    %%-------------- NODES ----------------------------------------------------
    usr_exportMaToZbrush("User<br>.<br>Export<br>Maya to ZBrush")
    act_exportMayaToZBrush("Action<br>.<br>Export<br>Maya to ZBrush")
    usr_openZBrush("User<br>.<br>Open<br>ZBrush")
    act_launchZBrush("Action<br>.<br>Launch<br>ZBrush")
    usr_importObj("User<br>.<br>Import<br>Obj")
    %%-------------- LINKS ----------------------------------------------------
    usr_exportMaToZbrush --> act_exportMayaToZBrush
    usr_openZBrush --> act_launchZBrush
    act_exportMayaToZBrush --> usr_importObj
    act_launchZBrush --> usr_importObj
    %%-------------- SUBGRAPH -------------------------------------------------
    %%-------------- CLASSES --------------------------------------------------
    classDef User fill:#FFEA6F
    classDef Rabbit fill:#FFB286
    classDef Action fill:#6FA2FF
    %%-------------- ASSIGN CLASSES -------------------------------------------
    class usr_exportMaToZbrush,usr_openZBrush,usr_importObj User
    %% class rbt_setTaskRdy,rbt_buildScene Rabbit
    class act_exportMayaToZBrush,act_launchZBrush Action
{{</mermaid>}}

{{<mermaid align="center" description="Export to Zbrush - Scenario">}}
graph LR
    linkStyle default interpolate basis
    %%-------------- NODES ----------------------------------------------------
    usr_exportMaToZbrush("User<br>.<br>Export<br>Maya to ZBrush")
    act_exportMayaToZBrush("Action<br>.<br>Export<br>Maya to ZBrush")
    usr_openZBrush("User<br>.<br>Open<br>ZBrush")
    act_launchZBrush("Action<br>.<br>Launch<br>ZBrush")
    usr_importObj("User<br>.<br>Import<br>Obj")
    %%-------------- LINKS ----------------------------------------------------
    usr_exportMaToZbrush --> act_exportMayaToZBrush
    usr_openZBrush --> act_launchZBrush
    act_exportMayaToZBrush --> usr_importObj
    act_launchZBrush --> usr_importObj
    %%-------------- SUBGRAPH -------------------------------------------------
    %%-------------- CLASSES --------------------------------------------------
    classDef User fill:#FFEA6F
    classDef Rabbit fill:#FFB286
    classDef Action fill:#6FA2FF
    %%-------------- ASSIGN CLASSES -------------------------------------------
    class usr_exportMaToZbrush,usr_openZBrush,usr_importObj User
    %% class rbt_setTaskRdy,rbt_buildScene Rabbit
    class act_exportMayaToZBrush,act_launchZBrush Action
{{</mermaid>}}


{{<mermaid align="center" description="Modeling Task Workflow.">}}
graph TD
    linkStyle default interpolate basis
    %% ------------------ NODES -----------------------------------------------
    user_status_ver_pdg_rev("User<br>.<br>Set SG Status Version<br>.<br>Pending Review")
    user_status_task_pdg_rev("User<br>.<br>Set SG Status Task<br>.<br>Pending Review")
    user_build_sg_playlist_review("User<br>.<br>Build SG Playslist<br>.<br>Review")
    user_status_task_pdg_qc("User<br>.<br>Set SG Status Task<br>.<br>Pending Quality Check")
    user_status_ver_apr("User<br>.<br>Set SG Status Version<br>.<br>Approved")
    user_annotate_ver("User<br>.<br>Annotate SG Version")
    user_status_task_rtk("User<br>.<br>Set SG Status Task<br>.<br>Retake")
    user_status_task_cmpt("User<br>.<br>Set SG Status Task<br>.<br>Complete")
    user_status_pf_apr("User<br>.<br>Set SG Status<br>Published File<br>.<br>Approved")
    user_publish_work_scene("User<br>.<br>Publish Work Scene")
    user_export_to_zbrush("User<br>.<br>Export To Zbrush")
    user_work_in_zbrush("User<br>.<br>Work in ZBrush")
    user_export_to_maya("User<br>.<br>Export To Maya")
    user_work_on_the_scene("User<br>.<br>Work on the Scene")
    user_open_published_scene("User<br>.<br>Open Published Scene")
    user_launch_maya("User<br>.<br>Launch Maya")
    user_go_in_project("User<br>.<br>Go in Project Env")
    user_set_sg_task_rdy("User<br>.<br>Set SG Status Task<br>.<br>Ready to Start")
    user_rev("User<br>.<br>Review")
    user_qc("User<br>.<br>Quality Check")
    %% ------------------ LINKS -----------------------------------------------
    user_set_sg_task_rdy --> sg_plugin_reflex_task_rdy
    sg_plugin_reflex_task_rdy -->|REST API| msg_task_rdy
    msg_task_rdy --> worker_build_scene
    worker_build_scene --> action_build_scene-farm
    action_build_scene-farm --> action_publish_build_scene-farm
    action_publish_build_scene-farm -->|REST API| msg_publish_build_scene
    msg_publish_build_scene --> worker_publish_build_scene
    worker_publish_build_scene --> action_send_published_scene_to_wkst
    action_send_published_scene_to_wkst -->|REST API| msg_published_scene_sended
    msg_published_scene_sended --> worker_send_notification_to_assigned
    worker_send_notification_to_assigned --> action_send_notification_to_assigned-farm
    action_send_notification_to_assigned-farm -.->|async| user_go_in_project
    %% USER PART
    user_go_in_project --> user_launch_maya
    user_launch_maya --> user_open_published_scene
    user_open_published_scene --> action_copy_published_scene_in_work_scene
    action_copy_published_scene_in_work_scene --> action_open_work_scene
    action_open_work_scene --> user_work_on_the_scene
    user_work_on_the_scene --> user_export_to_zbrush
    user_export_to_zbrush --> action_export_to_zbrush
    action_export_to_zbrush --> user_work_in_zbrush
    user_work_in_zbrush --> user_export_to_maya
    user_export_to_maya --> action_export_to_maya
    action_export_to_maya --> user_work_on_the_scene
    user_work_on_the_scene --> user_publish_work_scene
    user_publish_work_scene --> action_publish_zbrush_scene-local
    user_publish_work_scene --> action_publish_work_scene-local
    action_publish_work_scene-local -->|REST API| msg_work_scene_published
    action_publish_zbrush_scene-local -->|REST API| msg_zbrush_scene_published
    msg_work_scene_published --> worker_sync_file_to_server
    msg_work_scene_published --> worker_create_n_publish_alembic
    worker_create_n_publish_alembic --> action_create_n_publish_alembic-farm
    action_create_n_publish_alembic-farm -->|REST API| msg_publish_alembic
    msg_work_scene_published --> worker_render_turntable
    worker_render_turntable --> action_render_n_publish_published_scene-farm
    action_render_n_publish_published_scene-farm -->|REST API| msg_publish_images
    msg_publish_images --> worker_create_update_sg_version
    worker_create_update_sg_version --> action_create_update_sg_version-farm
    msg_publish_images --> worker_create_QT_n_publish
    worker_create_QT_n_publish --> action_create_QT_n_publish-farm
    action_create_QT_n_publish-farm -->|REST API| msg_publish_QT
    msg_publish_QT --> worker_create_sg_version
    worker_create_sg_version --> action_create_update_sg_version-farm
    action_create_update_sg_version-farm -->|REST API| msg_create_update_version
    msg_create_update_version --> worker_send_notification_to_artist
    worker_send_notification_to_artist --> action_send_notification_to_artist-farm
    %% REVIEW
    action_send_notification_to_artist-farm -.->|async| user_status_ver_pdg_rev
    action_send_notification_to_artist-farm -.->|async| user_status_task_pdg_rev
    user_status_ver_pdg_rev -.->|async| user_build_sg_playlist_review
    user_build_sg_playlist_review --> user_rev
    user_rev --> user_annotate_ver
    user_rev --> user_status_ver_apr
    user_rev --> user_status_task_rtk
    user_rev --> user_status_task_pdg_qc
    user_status_task_cmpt --> sg_plugin_reflex_task_complete
    sg_plugin_reflex_task_complete -->|REST API| msg_task_complete
    msg_task_complete --> worker_update_tasks_in_down_dependency
    user_status_task_pdg_qc --> user_qc
    user_qc --> user_status_task_cmpt
    user_qc --> user_status_pf_apr
    user_qc --> user_status_task_rtk
    user_status_pf_apr --> sg_plugin_reflex_pf_apr
    sg_plugin_reflex_pf_apr -->|REST API| msg_pf_apr
    msg_pf_apr --> worker_update_status_in_up_dependency
    msg_pf_apr --> worker_update_next_task
    worker_update_next_task --> action_update_next_task-farm
    worker_update_status_in_up_dependency --> action_propagate_sg_status_to_dependency

{{</mermaid>}}