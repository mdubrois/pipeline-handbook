---
title: "Playblast"
weight: 1000
description: "Description"
tags: ["multi"]
draft: false
---

| Card ||
|---|---|
| OS | windows, linux |
| Type | multi |
| DCC  | maya, houdini |


## Description
- Permet de générer des Images de qualité réduite mais plus rapidement.
- On utilise l'OpenGL généralement.
- Peut-être éxecuté Localement ou sur la RenderFarm.
- Nécessite des HUD
- Preserve Dependencies, on doit toujours avoir la scene qui génère les images de publiée et liée
- Rush Manager _(review local de playblast wip)_
