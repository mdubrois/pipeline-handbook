---
title: Schema
weight: 2500
description: "The different layers of Pipeline Schema."
tags: ["WIP"]
draft: false
---
{{% notice info %}}
    That's a very, very, wip Page. More a reflection than anything else
{{% /notice %}}

A pipeline is base on sevral layers like :

- Task Flow
- Data Flow
- Scene Description Schema

To design a Pipeline we must define two things, the [Tasks](/schema/task) dependencies and the Data Schema for the Assets and Shots.

{{<mermaid align="center" description="Pipeline">}}
graph TD
    linkStyle default interpolate basis
    task("Task")
    mservices("Micro-Services")
    data("Data Schema")
{{</mermaid>}}

**Task Dependencies**

{{<mermaid align="center" description="Task Dependencies.">}}
graph LR
    linkStyle default interpolate basis
    mod("modeling")
    tex("texturing")
    surf("surfacing")
    mod --> tex
    tex --> surf
{{</mermaid>}}

**Micro-Services Schema**

{{<mermaid align="center" description="Action per Task.">}}
graph LR
    linkStyle default interpolate basis
    user_publish_scene --> rest_api
    rest_api --> msg_broker
    msg_broker --> ms_publish_primary_scene
    msg_broker --> ms_publish_primary_abc
    msg_broker --> ms_publish_secondary

{{</mermaid>}}

**Scene Description Schema**

{{<mermaid align="center" description="Scene Description Schema.">}}
graph LR
    linkStyle default interpolate basis
    mod("modeling")
    tex("texturing")
    surf("surfacing")
    mod --> tex
    tex --> surf
{{</mermaid>}}

Like describe in our [Task](/schema/task) page. A Pipeline is very complex with a huge amount of dependencies driven by the Tasks at the top level.

Here we described the different layers of Pipeline Schema.

- Global Schema as Task Pipeline
- Shot
- Asset
- Micro-Services

----
## Our Goal
Before starting to explain ours differents Schema we must define what we want at the end, a **Shot**. A Shot with everything, color, fx, dynamics, hair, fur, cloth, optical effects.
A Shot like in the movie [Spring](https://www.blender.org/press/spring-open-movie/) made by the Blender Foundation.
<!-- ![spring-open](./_static/springopen_400.jpg) -->

To create a Shot we must first describe his content of this [Shot](/concept/shot).

{{%excerpt-include filename="concept/shot.en.md" panel="Shot description" /%}}

## Simple Shot Schema
A Simple Shot Schema can be represented like below. With the Content and the Assets Assembly.
{{<mermaid align="center" description="Simple Shot Schema.">}}
graph LR

    linkStyle default interpolate basis
    %% Nodes
    achar("Asset-Char")
    aprop("Asset-Prop")
    aset("Asset-Set")
    afx("Asset-Fx")
    alight("Asset-Light")
    ax("Asset-X")
    %%
    sassets("Assets")
    saprop("Asset-Prop")
    safx("Asset-Fx")
    salight("Asset-Light")
    sax("Asset-X")

    sovr("Overrides")

    sovr --> achar
    sovr --> aprop
    sovr --> aset
    sovr --> afx
    aset --> aprop
    sovr --> ax
    sovr --> alight

    sassets --> sovr
    sassets --> saprop
    sassets --> salight
    sassets --> safx
    sassets --> sax

    subgraph Asset
        achar
        aprop
        aset
        ax
        alight
        afx
    end
    subgraph Shot
        sovr
        sassets
        saprop
        salight
        safx
        sax
    end
{{</mermaid>}}

