---
title: "Security"
weight: 9000
description: "Not exactly a Concept."
tags: ["WIP"]
---

## Description

### MPAA
The [Motion Picture Association of America](https://www.mpaa.org) (MPAA) is an American trade association representing the five major film studios of Hollywood, and, as of January 22, 2019, streaming service giant, Netflix. Learn more on MPAA on [Wikipedia](https://en.wikipedia.org/wiki/Motion_Picture_Association_of_America). Below the list of members.

- [Disney](https://www.waltdisneystudios.com/)
- [Netflix](https://www.netflix.com/)
- [Paramount](https://www.paramount.com/)
- [Sony Pictures](http://www.sonypictures.com/corp/aboutsonypictures.html)
- [Universal](https://www.universalpictures.com/)
- [Warner Bros](http://www.warnerbros.com/)


The [MPAA](https://www.mpaa.org) is the institution who drive the "Best Practices" about **Security** in our world. The latest Guideline is available [here](https://www.mpaa.org/what-we-do/advancing-creativity/additional-resources/#content-protection-best-practices) at the end of the page.

- Direct Download of [MPAA-Best-Practices-Common-Guidelines-V4.05-Final.pdf](https://www.mpaa.org/wp-content/uploads/2019/05/MPAA-Best-Practices-Common-Guidelines-V4.05-Final.pdf)

### TPN
In 2018, the [MPAA](https://www.mpaa.org) joined forces with the [Content Delivery & Security Association](http://www.cdsaonline.org) (CDSA) to form the [Trusted Partner Network](www.TTPN.org) (TPN), an industry-wide film and television content security initiative designed to help prevent leaks, breaches, and hacks in the production pipeline prior to a film or TV show’s intended release.

The joint venture provides industry vendors with a voluntary cost-effective way to ensure that the security of their facilities, staffs, and workflows meets MPAA’s best practices and it accredits experienced auditors to conduct them globally. Through the TPN, the film and TV industry will elevate the security standards and responsiveness of the vendor community, while greatly expanding the number of facilities that are assessed annually.
