---
title: "Submitter"
weight: 1000
description: "Description"
tags: ["multi"]
draft: false
---

| Card ||
|---|---|
| OS | windows, linux |
| Type | multi |
| DCC  | maya, houdini |
| API  | True |
| GUI  | True |

## Description
- Permet d'envoyer des Jobs sur la RenderFarm
- API + GUI
