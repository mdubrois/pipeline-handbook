---
title: Pipeline Handbook
weight: 1000
description: "Description"
tags: []
draft: false
---
The purpose of this site is to describe a CG pipeline.

- How to set it up, what are its Technical Specifications, its Concepts.
- What are these Work and DataFlow Schemas.
- What Tools should be created. What's the Stack for this Pipeline.
- And finally how development must be organized to make this Pipeline.
