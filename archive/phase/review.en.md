---
title: "Review"
weight: 3000
description: "description"
draft: false
---

## Artistic
- Les artistes referred à un Lead, Superviseur.
- La chaîne de validation est en variable en fonction du type d'entity et de la tache. Graph de validation à définir. Example

{{<mermaid>}}
graph TD
    artist --> lead
    lead --> cg_sup
    cg_sup --> vfx_sup
    cg_sup --> ad
    vfx_sup --> director
    ad --> director
    director --> producer
{{< /mermaid >}}

## Technic
- Les artistes refer à un Lead, Prod, Superviseur, VFX Sup, Director. 
- La chaîne de commandement est variable. Graph de validation par valeur ajoutée.
