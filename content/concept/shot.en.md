---
title: "Shot"
weight: 1200
description: "A Shot, is a Scene record at one time."
tags: ["WIP"]
---

## Description
{{%excerpt%}}
A Shot, it's shoot a scene at a time. A Scene could be compose of Character, Animal, Prop, Vehicle, Environment, etc...
A Shot is also defined by start and stop the record of the Scene.

A Scene is technically composed of :

- meshes.
- texture.
- shaders.
- lights.
- fx like dusts, particles.
- opticaf fx, like dof, flares.

We could create the necessary elements in the Shot directly but for a Budget reason we usually decide to create Assets once and reuse them.

_But we must keep in mind that, sometimes producing an Asset directly in the Shot is easier._
{{% /excerpt%}}

## Features

{{% panel theme="danger" header="TODO" %}}
List the Shot's feature.
{{% /panel %}}