---
title: "Sanity Check"
weight: 1000
description: "Description"
tags: ["multi"]
draft: false
---

| Card ||
|---|---|
| OS | windows, linux |
| Type | multi |
| DCC  | maya, houdini |


## Description
- S'assure de la conformité d'une Scene, Element.
- Quand on valid un Asset, on le valid a un instant T avec une version. On doit stocker avec quelle version on a validé comme pour du code. Ça nous permet de savoir le contenu du sanity check a l’instant T.