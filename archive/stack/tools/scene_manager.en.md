---
title: "Scene Manager"
weight: 1000
description: "Description"
tags: ["multi"]
draft: false
---

| Card ||
|---|---|
| OS | windows, linux |
| Type | multi |
| DCC  | maya, houdini |


## Description
- Permet d'interagir avec les Elements d'une Scene.
- CRUD des Elements
