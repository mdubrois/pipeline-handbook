---
title: "Task"
weight: 2000
description: "A Task is to do something at a time. Usually done by a human."
tags: ["WIP"]
---

## Description
A Task is usually owned by the Production Tracking Crew.
That's allow them to manage the manufacturing of the Project and ensure to deliver the Project on time.


- A Task is an action at a time.
- A Task can be done by a human or a machine.
- At the end of this Task something can be triggered.
- A Sub-Task is an internal step in the Task. A Sub-Task can't trigger anything outside the Task.
- When you group several Tasks that look alike together, it's a Step.


Before Task, we must also define :

- The severals `status` for this Task.
- The status `status` flow.
- The `action` allowed for this Task.
- The `tool` allowed for this Task.
- The default `action` flow.

_Often, a Task represents the assignment of an Artist to do something._


## Features
The Task have the spec below :

- **Bid** / _How many budget for doing this Task._
- **Start, End** / _On which period we expect to doing this Task._
- **Status** / _That's allow everybody to know the progression._
- **Description** / _A simple description to explain teh Task._
- **Timelog** / _How many the Artist have spent time on it._
- **Sub-Task** / _Sometimes it's more easy to cut your big Task into Sub-Task for tracking._
- **Dependencies** / _We also want dependencies between Tasks to know when we could start another Task._
    - Look at [SG Task](http://developer.shotgunsoftware.com/python-api/tasks/task_dependencies.html) for technical detail.

With `dependencies` the `Task` looks like a `Pipeline`.

{{<mermaid align="center" description="Task dependencies.">}}
graph LR
    linkStyle default interpolate basis
    ta("Task A")
    tb("Task B")
    tc("Task C")
    td("Task D")

    ta --> tb
    tb --> tc
    td --> tb

    subgraph Step
    ta
    td
    end
{{</mermaid>}}

{{% notice info %}}
That's why you must talk with the Production Tracking Crew each time you want to re-design the Pipeline.
Tracking Task and Pipeline Task must have a bi-directional link on their Status and on their Dependencies.
{{% /notice %}}


### Status
Usually the basic Status are :

- **Waiting to Start** / _We are waiting every requirement before starting to work._
- **Ready to Start** / _We are waiting every requirement before starting to work._
- **In Progress** / _We are waiting every requirement before starting to work._
- **Pending Review** / _We are waiting every requirement before starting to work._
- **Approved** / _We are waiting every requirement before starting to work._
- **Omit** / _We are waiting every requirement before starting to work._
- **On Hold** / _We are waiting every requirement before starting to work._

But in reality we want :

{{<mermaid align="center" description="Dependencies of task statuses.">}}
graph TD
    linkStyle default interpolate basis
    %% ---------------- NODES -------------------------------------------------
    wtg("waiting to Start")
    rdy("Ready to Start")
    ip("In Progress")
    cpu("Computing")
    rtk("Retake")
    prev("Pending Review")
    rev("Review")
    pqc("Pending Quality Check")
    qc("Quality Check")
    cpt("Complete")
    omt("Omit")
    hld("On Hold")
    %% ---------------- LINKS -------------------------------------------------
    wtg --> rdy
    rdy --> ip
    ip --> cpu
    cpu --> prev
    prev --> rev
    rev --> pqc
    rev -.-> rtk
    pqc --> qc
    qc -.-> rtk
    rtk --> ip
    qc --> cpt
    %% ---------------- SUBGRAPH ----------------------------------------------
    subgraph Task Status
        wtg
        rdy
        prev
        rev
        pqc
        qc
        cpt
        ip
        cpu
        rtk
        omt
        hld
    end
{{</mermaid>}}

#### Review & Quality Check - Internal Process
{{<mermaid align="center" description="Review & Quality Check - Internal Process.">}}
graph TD
    linkStyle default interpolate basis
    %% ---------------- NODES -------------------------------------------------
    aprt{"Technical Approval"}
    apra{"Artistic Approval"}
    pdc("Producer")
    dir("Director")
    da("Artistic Director")
    ad("Animation Director")
    vsup("VFX Sup")
    cgsup("CG Sup")
    hod("Head of Department")
    ld("Lead")
    ldt("Lead")
    hodt("Head of Department")
    %% ---------------- LINKS -------------------------------------------------
    ld --> hod
    hod --> cgsup
    cgsup --> vsup
    vsup --> ad
    vsup --> da
    ad --> dir
    da --> dir
    dir --> pdc
    pdc --> apra
    ldt --> hodt
    hodt --> aprt
    %% ---------------- SUBGRAPH ----------------------------------------------
    subgraph Review
        ld
        hod
        cgsup
        vsup
        ad
        da
        dir
        pdc
        apra
    end
    subgraph Quality Check
        ldt
        hodt
        aprt
    end
{{</mermaid>}}

## Validation Dependency per Task
{{% panel theme="danger" header="TODO" %}}
How to define clearly the validation dependencies per Task.
{{% /panel %}}

## Use Case
### Task in Animation
At the animation Step, we want a Sub-Task under the main anim Task.
To better track the status.
The Sub-Task will be:

* Block
* Spline
* Polish
* Refine
* Deliver


