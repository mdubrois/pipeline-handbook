---
title: "The Crew"
weight: 1000
description: "Description"
tags: ["WIP"]
draft: false
---


{{<mermaid align="center" description="Pipeline">}}
graph LR
    %% linkStyle default interpolate basis
    cto("CTO<br>")
    hod("Head of Dev")
    hops("Head of Pipeline - Serie")
    hopf("Head of Pipeline - Film")
    d1("Dev 1")
    d2("Dev 2")
    d3("Dev 3")
    tds1("TD Serie - Projext X")
    tds2("TD Serie - Projext X")
    tds3("TD Serie - Projext X")
    tds4("TD Serie - Projext X")
    tdf1("TD Film - Environment<br>Prop, Set, Dmp")
    tdf2("TD Film - Char / Anim<br>Char, Rig, Anim")
    tdf3("TD Film - Anim<br>Edit, Previz, Anim")
    tdf4("TD Film - CFX / FX")
    tdf5("TD Film - SLC<br>Surf, Light, Comp")
    %% ------------------------------------
    cto --> hod
    cto --> hops
    cto --> hopf
    %% ------------------------------------
    hod --> d1
    hod --> d2
    hod --> d3
    %% ------------------------------------
    hops --> tds1
    hops --> tds2
    hops --> tds3
    hops --> tds4
    %% ------------------------------------
    hopf --> tdf1
    hopf --> tdf2
    hopf --> tdf3
    hopf --> tdf4
    hopf --> tdf5

{{</mermaid>}}

- mod : maya zbrush
- surf : mari substance
- rig : maya
- cfx : maya houdini
- fx : maya houdini nuke
- dmp : maya nuke
- anim : maya
- previz : maya
- lighting : maya katana guerilla nuke
- comp : nuke
- di : nuke resolve

- Anim :
- Look :



