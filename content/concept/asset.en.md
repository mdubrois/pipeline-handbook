---
title: "Asset"
weight: 1000
description: "An Entity is an Item of a specific type, used to produce something."
tags: ["WIP"]
draft: false
---

An Entity is an Item of a specific type, used to produce something.

{{% notice note %}}
    For example an Asset, a Sequence, a Shot, a Task.
    This Entities can have a sub-type like Character, Prop, Set, Fx for an Asset.
{{% /notice %}}

## Variant
* On peut avoir des variations à chaque Step de fabrication.
* Une Variation est une variation Artistique.
    * Modeling variation (default, broken).
    * Shading variation (bleu, rouge).
    * Rig variation (default, broken).

## Level of Detail / LOD
* On peut avoir des LOD à chaque Step de fabrication.
* Une LOD est une Variation Technique.
    * Modeling lod (default, low, high, veryHigh).
    * Shading variation (default, low, high, veryHigh).
    * Rig variation (default, layout, facial, body, full).
        * On peut sortir un rig avec ou moins tout de branché. Dans le cas d'un Shot en Closeup sur le visage on ne veut que le facial, pas tout le reste.
    * Une LOD peut*être ou pas générative par rapport à une autre LOD.

## CRUD, create, read, update, delete
* Un élément d’une entitée (Asset/Shot) peut*être crée, lu, overridé, supprimé par n’importe quel step de fabrication.
    * Camera, édité par plusieurs étapes (Previz, Layout, Anim, Finalling)
    * Previz, Anim, Stereo, Final Set Dress 
    * Modeling, UV, Facial

## Representation/Visualisation
* Les steps de fabrication n’ont en général pas besoin de travailler sur tous les éléments de l’entité.
    * On veut pouvoir **isoler** les **éléments** sur lesquels on veut travailler à un certain **Step** dans une certaine **representation**. 
    * On veut avoir les autres éléments dans une autre représentation plus simple/légère.
    * En fonction du Step, on veut avoir les éléments dans une certaine représentation pour un calcul d'image en vue d'une review intern, extern.
* Pouvoir identifier ce qui doit être cacher dans le viewport pour des raisons technique de ce qui doit être cacher pour des raisons esthétiques
    * Playblast cache un mur mais on le veut au rendu.