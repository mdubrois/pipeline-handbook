---
title: "Action"
weight: 10000
description: "An Action is when you do something in the Pipeline."
tags: ["WIP"]
---

# Description
An `action` is to do something in the Pipeline. An `action`can be for example :

* Publish a File
* Open a File.
* Export an Animation.

Everything in the Pipeline turns around the `action`.

Each Action always have :

* [pre-action](#pre-action)
* [action](#action)
* [post-action](#post-action)

That's allow us to create dependencies between **action**.
Look at the Use Case [Publish a File](#publish-a-file) for more details.

#### pre-action
The `pre-action`allow to execute something before the main `action`.
Usually it's to ensure everything is ok before executing the `action`.
If the `pre-action` failed, you can't execute the `action`.

#### action
The `action` is the main part of the job.

#### post-action
The `post-action` execute a job after the `action`.
If the `action` failed, the `post-action` will not be executed.


# Use Case
## Publish a File
In this case we have the `action`**publish** with the description below.

``` yaml
publish:
    pre-action:
        - sanity_check
    action:
        - _exec_main
    post-action:
        - push_on_server
```

In `pre-action`, that's call the `action` **sanity_check**, with the schema below.
Which execute only the main part of the **sanity_check**.
``` yaml
sanity_check:
    pre-action:
        - null
    action:
        - _exec_main
    post-action:
        - null
```
And in `post-action`, that's call the `action` **push_on_server**, with the schema below.
Which execute only the main part of the **push_on_server**.
``` yaml
push_on_server:
    pre-action:
        - null
    action:
        - _exec_main
    post-action:
        - null
```

So, at the end our workflow will be :

{{<mermaid>}}
graph TD

    subgraph Action Publish
        p_pa("pre-action")
        p_a("action")
        p_ap("post-action")
    end
    subgraph Action Sanity Check
        sc_pa("pre-action")
        sc_a("action")
        sc_ap("post-action")
    end
    subgraph Action Push On Server
        pos_pa("pre-action")
        pos_a("action")
        pos_ap("post-action")
    end

    p_pa --> sc_pa
    sc_pa --> sc_a
    sc_a --> sc_ap
    sc_ap --> p_a
    p_a --> p_ap
    p_ap --> pos_pa
    pos_pa --> pos_a
    pos_a --> pos_ap
{{< /mermaid >}}
