---
title: Task
weight: 4000
description: "A Shot Schema from the Technical point of view."
tags: []
draft: false
---

|   |   |   |   |   |
|---|---|---|---|---|
|   |   |   |   |   |
|   |   |   |   |   |
|   |   |   |   |   |


Art Design
Art Facial
Art Look
Modeling
Modeling Facial
Texturing
Shading
Cfx Hair
Cfx Cloth
Rig-Low
Rig-High
Fx
    %% ---- DESC ASSET PROP ----
Art Design
Art Look
Modeling")
    aptex("Texturing")
    apshd("Shading")
    apcfxh("Cfx Hair")
    apcfxc("Cfx Cloth")
    apriglo("Rig-Low")
    aprighi("Rig-High")
    apfx("Fx")
    apdmp("Dmp")
    %% ---- DESC ASSET SET/ENV ----
    asartd("Art Design")
    asass("Assembly")
    assetd("SetDress")
    %% ---- DESC SHOT ----
    sstb("Storyboard")
    sedits("Edit-Animatic")
    sprz("Previz")
    seditp("Edit-Previz")
    sedita("Edit-Anim")
    sediti("Edit-Image")
    slay("Layout")
    sanm("Anim")
    sfx("Fx")
    scfxh("Cfx Hair")
    scfxc("Cfx Cloth")
    sfin("Finalling")
    sstereo("Cam Stereo")
    sdmp("Dmp")
    splgt("Pre-Lighting")
    slgt("Lighting")
    slgts("Lighting-Stereo")
    scomp("Compositing")
    scomps("Compositing Stereo")
    scg("Color Grading")




Below the Task dependencies for a Show.
{{% notice note %}}
    Keep in mind it's not a Data Flow.
{{% /notice %}}
{{<mermaid align="center" description="Task Dependencies for a Shot.">}}
graph LR

    linkStyle default interpolate basis
    %% ---- DESC ASSET CHAR ----
    acarts("Art Design")
    click acarts callback "Tooltip for a callback"
    acartf("Art Facial")
    acartl("Art Look")
    acmod("Modeling")
    acmodf("Modeling Facial")
    actex("Texturing")
    acshd("Shading")
    accfxh("Cfx Hair")
    accfxc("Cfx Cloth")
    acriglo("Rig-Low")
    acrighi("Rig-High")
    acfx("Fx")
    %% ---- DESC ASSET PROP ----
    apartd("Art Design")
    apartl("Art Look")
    apmod("Modeling")
    aptex("Texturing")
    apshd("Shading")
    apcfxh("Cfx Hair")
    apcfxc("Cfx Cloth")
    apriglo("Rig-Low")
    aprighi("Rig-High")
    apfx("Fx")
    apdmp("Dmp")
    %% ---- DESC ASSET SET/ENV ----
    asartd("Art Design")
    asass("Assembly")
    assetd("SetDress")
    %% ---- DESC SHOT ----
    sstb("Storyboard")
    sedits("Edit-Animatic")
    sprz("Previz")
    seditp("Edit-Previz")
    sedita("Edit-Anim")
    sediti("Edit-Image")
    slay("Layout")
    sanm("Anim")
    sfx("Fx")
    scfxh("Cfx Hair")
    scfxc("Cfx Cloth")
    sfin("Finalling")
    sstereo("Cam Stereo")
    sdmp("Dmp")
    splgt("Pre-Lighting")
    slgt("Lighting")
    slgts("Lighting-Stereo")
    scomp("Compositing")
    scomps("Compositing Stereo")
    scg("Color Grading")

    %% ---- LINK ASSET CHAR ----
    acarts --> acmod
    acarts --> acartf
    acartf --> acmodf
    acarts --> acartl
    acartl --> actex
    acmod --> acmodf
    acmod --> actex
    actex --> acshd
    acmod --> acriglo
    acmod --> acrighi
    acmodf --> acrighi
    acshd --> acrighi
    acshd -.->|optional| acfx
    acmod -.->|optional| accfxh
    acmod --> accfxc
    accfxh --> acrighi
    acrighi -.->|optional| accfxc
    acmod -.->|optional| acfx
    acfx -.->|optional| acrighi

    %% ---- LINK ASSET PROP ----
    apartd --> apmod
    apartd --> apdmp
    apartd --> apartl
    apartl --> aptex
    apmod --> aptex
    aptex --> apshd
    apmod -.->|optional| apdmp
    apmod --> apriglo
    apmod --> aprighi
    apshd --> aprighi
    apshd -.->|optional| apfx
    apmod -.->|optional| apcfxh
    apcfxh -.->|optional| aprighi
    aprighi --> apcfxc
    apmod --> apcfxc
    apmod -.->|optional| apfx
    apfx -.->|optional| aprighi

    %% ---- LINK ASSET SET ----
    asartd --> asass
    apmod --> asass
    apshd --> asass
    apriglo --> asass
    aprighi --> asass
    asass --> assetd

    subgraph Asset Char/Animal
        subgraph Art
            acarts
            acartf
            acartl
        end
        subgraph Modeling
            acmod
            acmodf
        end
        subgraph Cfx
            accfxh
            accfxc
        end
        subgraph Surfacing
            actex
            acshd
        end
        subgraph Rig
            acriglo
            acrighi
        end
        subgraph Fx
            acfx
        end
    end
    subgraph Asset Prop
        subgraph Art
            apartd
            apartl
        end
        subgraph Modeling
            apmod
        end
        subgraph Cfx
            apcfxh
            apcfxc
        end
        subgraph Surfacing
            aptex
            apshd
        end
        subgraph Rig
            apriglo
            aprighi
        end
        subgraph Fx
            apfx
        end
        subgraph Matte-Painting
            apdmp
        end
    end
        subgraph Asset Set
        subgraph Art
            asartd
        end
        subgraph Assembly
            asass
            assetd
        end
    end
    %% -----------------------------
    %% Link Shot
    sstb --> sedits
    assetd --> sprz
    sedits --> sprz
    apriglo --> sprz
    acriglo --> sprz
    sprz --> seditp
    seditp --> slay
    acrighi --> slay
    aprighi --> slay
    slay --> sanm
    sanm --> sedita
    sanm -.->|optional| scfxh
    sanm -.->|optional| scfxc
    sanm -.->|optional| sfx
    sanm --> sfin
    acshd --> splgt
    apshd --> splgt
    sanm --> splgt
    apcfxh --> scfxh
    apcfxc --> scfxc
    accfxh --> scfxh
    accfxc --> scfxc
    splgt --> slgt
    scfxh --> slgt
    scfxc --> slgt
    acfx --> sfx
    sfx --> slgt
    sfin --> slgt
    sanm --> sstereo
    sfx --> sstereo
    apdmp --> sdmp
    apdmp -.->|optional| sprz
    apdmp -.->|optional| slay
    sdmp --> slgt
    slgt --> scomp
    sstereo --> slgts
    slgt --> slgts
    sdmp --> scomp
    scomp --> sediti
    scomp --> slgts
    slgts --> scomps
    scomp --> scomps

    scomp --> scg
    scomps --> scg

    subgraph Shot
        subgraph Prepa
            sstb
        end
        subgraph Previz
            sprz
            slay
        end
        subgraph Edit
            sedits
            seditp
            sedita
            sediti
        end
        subgraph Anim
            sanm
        end
        subgraph Cfx
            scfxh
            scfxc
            sfin
        end
        subgraph Fx
            sfx
        end
        subgraph Lighting
            splgt
            slgt
            slgts
        end
        subgraph Matte-Painting
            sdmp
        end
        subgraph Compositing
            scomp
            scomps
        end
        subgraph Stereo
            sstereo
        end
        subgraph DI
            scg
        end
    end
    %% Color
    classDef Previz fill:#c0d6e4
    classDef Edit fill:#c0d6e4
    classDef Lighting fill:#6c78a7
    classDef Cfx fill:#ff9ca6
    classDef Fx fill:#c0e4e0
    classDef Matte-Painting fill:#ffb6c1
    classDef Compositing fill:#ffe4e1
    classDef Stereo fill:#e6e6fa
    classDef DI fill:#c0d6e4
    classDef Lighting fill:#d3ffce
    classDef Anim fill:#d0f4f4
    classDef Previz fill:#eed5ff
    classDef Prepa fill:#ffc0d7
    classDef Rig fill:#fff9c1
    classDef Surfacing fill:#cfebd2

    class sedits,seditp,sedita,sediti Edit
    class splgt,slgt,slgts Lighting
    class scg DI
    class sstereo Stereo
    class scomp,scomps Compositing
    class sdmp Matte-Painting
    class sfx,acfx Fx
    class scfxh,scfxc,sfin,accfxh,accfxc Cfx
    class sanm Anim
    class sprz,slay Previz
    class sstb Prepa
    class acriglo,acrighi Rig
    class actex,acshd Surfacing

{{</mermaid>}}