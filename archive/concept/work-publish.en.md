---
title: "Work, Publish, Approved"
weight: 10000
description: "The different Concept used by the Pipeline."
draft: true
---

## Work / Commit
- Une work scene est une scene de travail en cours,
- Elle peut être supprimée à tout moment sans aucune conséquence
- On ne perd que le travail ajouté par rapport à la Scene Publiée

## Publish / Commit + Push
- Une Scene Publish est une Scene Work publiée.
- Elle est enregistrée dans la base de donnée
- On peut lui attribuer un statut
- C'est à partir de cette Scene que des processes peuvent être lancé.
- Once it’s published the other user can access to this file.
- It’s published but it’s not usable in production.
- To be usable, the publish should pass through the CI. If the publish do not failed. It could be submitted for a technical approval.

## Approved / CI + Code Review
- When the file is technically approved. And artistically approved. It can be used be the others.
- Technically Approved it’s like CI quality test
- Artistically Approved it’s like Code Rebiew Approved.

As usual this is the main way sometimes we need to bypass this concept.
And set the latest publish as an Approved.
Everything should be define in a file like the gitlabci do.