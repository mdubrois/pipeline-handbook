---
title: "Monitoring"
weight: 3000
description: "description"
tags: ["WIP"]
draft: false
---

## Monitoring
### Pipeline
- We must be able to `time` each execution of an [action](/concept/action).
- We must be able to create `report` based on Year, Month, Project.
- We must be able to automatically detect a time issue for an [action](/concept/action).
- By monitoring these execution times it allows us to detect and optimize the slower parts in our pipeline.

### Licence
- Combien on en a par Soft.
- Combien son utilisé actuellement.
- On doit pouvoir faire des rapports d'utilisation par année, mois, prod.

#
- Monitoring Utilisation des packages