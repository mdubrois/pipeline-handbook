---
title: "Asset Instance"
weight: 1500
description: "An Asset Instance is an Instance of an Asset in Master or Shot Context."
tags: ["WIP"]
draft: false
---

## Description

An Asset Instance is an Instance of an Asset. It's usually used in context of Set, Master or Shot.

In a Shot Context when you take an Asset in Reference, we always suffix it with a number. That's allow us to take another same Asset in reference.

{{% notice note %}}
    An Asset can be taken in Reference by a Set.
    And this Set can also be taken in Reference by a Shot.
{{% /notice %}}

{{<mermaid align="center" description="Simple Asset Schema.">}}
graph LR
    linkStyle default interpolate basis
    %% NODES
    pa("Asset<br>Prop A")
    pb("Asset<br>Prop B")
    sa("Asset<br>Set A")
    pa1("Asset Instance<br>Prop A 001")
    pa2("Asset Instance<br>Prop A 002")
    sh_sa1("Asset Instance<br>Set A 001")
    sh_pa1("Asset Instance<br>Prop A 001")
    sh_pb1("Asset Instance<br>Prop B 001")
    sh("Shot<br>seq001_sh001")

    %% LINKS
    pa --> pa1
    pa --> pa2
    pa1 --> sa
    pa2 --> sa

    pa --> sh_pa1
    pb --> sh_pb1
    sa --> sh_sa1
    sh_sa1 --> sh

    sh_pa1 --> sh
    sh_pb1 --> sh
    %% SUBGRAPH
    subgraph Assets
        pa
        pb
        subgraph Set A
            pa1
            pa2
            sa
        end
    end
    subgraph Shot seq001_sh001
        sh_pa1
        sh_pb1
        sh
        sh_sa1
    end
    %% COLOR
    classDef AssetInstance fill:#cfebd2
    class pa1,pa2,sh_sa1,sh_pa1,sh_pb1 AssetInstance
{{</mermaid>}}


