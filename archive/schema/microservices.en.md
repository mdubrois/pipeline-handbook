---
title: Microservices
weight: 2500
description: "Blah about Microservices"
tags: ["WIP"]
draft: true
---

## Schema

{{<mermaid align="center" description="Microservices Schema.">}}
graph TD
    linkStyle default interpolate basis
    %% ---------------------- NODES -------------------------------------------
    %% ---------------------- LINKS -------------------------------------------
    get_published_file
    %% ---------------------- SUBGRAPH ----------------------------------------

{{</mermaid>}}

## List of Microservices

| Name | Descprition | Doc |
| ---- | ---- | ---- |
| Get Published File | Retrieve Published File like sg.find | [doc](link) |
| Create Published File | Retrieve Published File like sg.find | [doc](link) |